export enum Orientation {
  N = 'N',
  E = 'E',
  W = 'W',
  S = 'S',
}

export interface GridSize {
  x: number;
  y: number;
}

export class VacuumCleaner {
  private x: number;
  private y: number;
  private orientation: Orientation;
  private gridSize: GridSize;

  constructor(gridSize: GridSize, x: number, y: number, orientation: Orientation) {
    this.gridSize = gridSize;
    this.x = x;
    this.y = y;
    this.orientation = orientation;
  }

  private turnRight(): void {
    switch (this.orientation) {
      case Orientation.N:
        this.orientation = Orientation.E;
        break;
      case Orientation.E:
        this.orientation = Orientation.S;
        break;
      case Orientation.S:
        this.orientation = Orientation.W;
        break;
      case Orientation.W:
        this.orientation = Orientation.N;
        break;
    }
  }

  private turnLeft(): void {
    switch (this.orientation) {
      case Orientation.N:
        this.orientation = Orientation.W;
        break;
      case Orientation.W:
        this.orientation = Orientation.S;
        break;
      case Orientation.S:
        this.orientation = Orientation.E;
        break;
      case Orientation.E:
        this.orientation = Orientation.N;
        break;
    }
  }

  private moveForward(): void {
    switch (this.orientation) {
      case Orientation.N:
        if (this.y < this.gridSize.y) {
          this.y += 1;
        }
        break;
      case Orientation.E:
        if (this.x < this.gridSize.x) {
          this.x += 1;
        }
        break;
      case Orientation.S:
        if (this.y > 0) {
          this.y -= 1;
        }
        break;
      case Orientation.W:
        if (this.x > 0) {
          this.x -= 1;
        }
        break;
    }
  }

  public executeInstructions(instructions: string): void {
    for (const instruction of instructions) {
      switch (instruction) {
        case 'D':
          this.turnRight();
          break;
        case 'G':
          this.turnLeft();
          break;
        case 'A':
          this.moveForward();
          break;
      }
    }
  }

  public getPosition(): string {
    return `Position finale : (${this.x}, ${this.y}), orientation : ${this.orientation}`;
  }
}

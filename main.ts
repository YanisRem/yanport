import * as readlineSync from 'readline-sync';
import { GridSize, Orientation, VacuumCleaner } from './src/vacuumCleaner';


interface VacuumPosition {
  x: number;
  y: number;
  orientation: string;
}

//Saisie de la grille
function getGridSizeFromUser(): GridSize {
  const x = readlineSync.questionInt('Entrez la taille de la grille sur l\'axe x : ');
  const y = readlineSync.questionInt('Entrez la taille de la grille sur l\'axe y : ');
  return { x: Number(x), y: Number(y) };
}


//Saisi de la position et de l'orientation de l'aspirateur
function getVacuumPositionFromUser(): VacuumPosition {
  console.log(`Veuillez saisir la position de l\'aspirateur et son orientation`);
  const posX = readlineSync.questionInt('Position x : ');
  const posY = readlineSync.questionInt('Position y : ');
  const orientation = readlineSync.question('Orientation (N, E, S, W) : ');

  return { x: posX, y: posY, orientation };
}

//Saisi des instruction à suivre
function getInstructionsFromUser(): string {
  const instruction = readlineSync.question('Veuillez saisir les instructions : ');

  return instruction;
}


async function main(): Promise<void> {
  const gridSize = await getGridSizeFromUser();
  console.log(`Taille de la grille saisie : ${gridSize.x} x ${gridSize.y}`);

  const vacuumPosition = await getVacuumPositionFromUser();
  console.log(`Position de l'aspirateur saisie : (${vacuumPosition.x}, ${vacuumPosition.y}), orientation : ${vacuumPosition.orientation}`);

  const instructions = await getInstructionsFromUser();
  console.log(`Instructions saisies : ${instructions}`);

  const orientation = vacuumPosition.orientation as Orientation;

  const vacuumCleaner = new VacuumCleaner(gridSize, vacuumPosition.x, vacuumPosition.y, orientation);
  vacuumCleaner.executeInstructions(instructions);

  const finalPosition = vacuumCleaner.getPosition();
  console.log(finalPosition);
}

main();

# Projet Aspirateur Automatique

Ce projet implémente une application en TypeScript permettant de simuler un aspirateur automatique se déplaçant dans une grille rectangulaire.


## Installation

1. Assurez-vous d'avoir Node.js installé sur votre machine.
2. Cloner le fichier sur votre machine.


## Utilisation

1. Ouvrez un terminal et accédez au répertoire du projet.

2. Installez les dépendances du projet en exécutant la commande suivante : "npm install"

3. Compiler le projet typescript avec la commande : "npx tsc"

3. Exécutez le projet en exécutant la commande suivante : "node main.js"
Cela lancera l'application de l'aspirateur automatique.

4. Suivez les instructions affichées dans la console pour saisir les dimensions de la grille, la position initiale de l'aspirateur et les instructions pour le déplacement.

5. L'application affichera la position finale de l'aspirateur après avoir exécuté les instructions.


## Structure du projet

- Le fichier `main.ts` contient le point d'entrée de l'application où les interactions avec l'utilisateur sont gérées.
- Le fichier `src/VacuumCleaner.ts` contient la classe `VacuumCleaner` qui représente l'aspirateur automatique et ses fonctionnalités.


## Auteur

Ce projet a été développé par REMOND Yanis.


## Remarque

Ce projet a été réalisé dans le cadre d'un exercice et n'est pas destiné à être utilisé en production.
Il a pour but de démontrer l'utilisation de TypeScript pour la modélisation d'un aspirateur automatique.

